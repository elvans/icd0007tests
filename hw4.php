<?php

require_once('common.php');

const BASE_URL = 'http://localhost/icd0007';

class Hw4Tests extends HwTests {

    function baseUrlRespondsWithNoErrors() {
        $this->assertTrue($this->get(BASE_URL));
        $this->assertResponse(200);

        $source = $this->getBrowser()->getContentAsText();

        if (preg_match('/Fatal error:.*Stack trace:/', $source)) {
            $this->fail($source . PHP_EOL);
        }
    }

    function listPageHasMenuWithCorrectLinkIds() {
        $this->get(BASE_URL);
        $this->assertLinkById('list-page-link');
        $this->assertLinkById('add-page-link');
    }

    function allLinksPointToTheSameScript() {

        $this->get(BASE_URL);

        // all links should be in the following format
        //   end with /
        //   end with ?key1=value1&key2=value2&...
        //   end with /index.php
        //   end with index.php?key1=value1&key2=value2&...

        $ends_with_slash = '^[^.]+\/$';
        $ends_with_params = '^[^?]+\/\?[-=&\w]*$';
        $ends_with_index = '^[^.]+\/index\.php$';
        $ends_with_index_and_params = '^[^.]+\/index\.php\?[-=&\w]*$';

        $pattern = "/($ends_with_slash)|($ends_with_params)"
                 . "|($ends_with_index)|($ends_with_index_and_params)/";

        $message = 'Front Controller pattern expects all links '
                 . 'to be in certain format';

        $this->assertLinkById('list-page-link',
            new PatternExpectation($pattern), $message);
        $this->assertLinkById('add-page-link',
            new PatternExpectation($pattern), $message);
    }

    function addPageHasCorrectElements() {
        $this->get(BASE_URL);

        $this->clickLinkById('add-page-link');

        $this->assertField('firstName');
        $this->assertField('lastName');
        $this->assertField('phone1');
        $this->assertField('phone2');
        $this->assertField('phone3');

        $this->assertField('submit-button');
    }

    function submittingFormAddsPersonToList() {

        $this->get(BASE_URL);

        $this->clickLinkById('add-page-link');

        $person = getSampleData();

        $this->setFieldByName('firstName', $person->firstName);
        $this->setFieldByName('lastName', $person->lastName);
        $this->setFieldByName('phone1', $person->phone1);
        $this->setFieldByName('phone2', $person->phone2);
        $this->setFieldByName('phone3', $person->phone3);

        $this->clickSubmitByName('submit-button');

        $this->assertText($person->firstName);
        $this->assertText($person->lastName);
        $this->assertText($person->phone1);
        $this->assertText($person->phone2);
        $this->assertText($person->phone3);
    }

    function makesRedirectAfterFormSubmission() {
        $this->get(BASE_URL);

        $this->clickLinkById('add-page-link');

        $this->setMaximumRedirects(0);

        $this->clickSubmitByName('submit-button');

        $this->assertResponse(302);

        $source = $this->getBrowser()->getContentAsText();

        $this->assertNoPattern('/[<>\w\d]/',
            "Should not print any output along with Location header " .
            "but output was: \n $source");
    }
}

(new Hw4Tests())->run(new PointsReporter());
